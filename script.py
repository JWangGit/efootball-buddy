import requests
from bs4 import BeautifulSoup
import json

# print("starting script")
# Get the HTML of the PESDB page
response = requests.get("https://pesdb.net/pes2022/?all=1")

# print(response)

print(response.text)

soup = BeautifulSoup(response.content, "html.parser")

# Find all the player tables on the page
player_tables = soup.find_all("table", class_="table")

# print(player_tables)

# Loop through each player table
for player_table in player_tables:
    # Get the player's name
    player_name = player_table.find("td", class_="name").text

    # Get the player's nationality
    player_nationality = player_table.find("td", class_="nationality").text

    # Get the player's position
    player_position = player_table.find("td", class_="position").text

    # Get the player's overall rating
    player_rating = player_table.find("td", class_="overall").text

    # Print the player's information
    print(f"Name: {player_name}, Nationality: {player_nationality}, Position: {player_position}, Overall Rating: {player_rating}")

# print("Done")

